// Настройка фильтра //

var acc = document.getElementsByClassName("filter-item-drop");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("filter-item-drop--active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
        content.style.display = "block";
    }
  });
}

// Настройка тегов //

const tag = document.querySelectorAll(".tag_visibility");
const tag_close = document.querySelector(".btn-close");

tag_close.addEventListener("click",  function(event) {
  event.preventDefault();
  if (tag_close.innerHTML === "Свернуть") {
    tag_close.innerHTML = "Показать еще";
  } else {
    tag_close.innerHTML = "Свернуть"
  }
  tag.forEach((el) => {
    el.classList.toggle("tag-hidden");
  })
})

var header = document.getElementById("tags");
var btn_tag = header.getElementsByClassName("tag__link");
for (var i = 0; i < btn_tag.length; i++) {
  btn_tag[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("tag__link--active");
    current[0].className = current[0].className.replace(" tag__link--active", "");
    this.className += " tag__link--active";
  });
}

// Настройка выпадающего меню //

const selectSingle = document.querySelector('.select');
const selectSingle_title = selectSingle.querySelector('.select__title');
const selectSingle_labels = selectSingle.querySelectorAll('.select__label');

selectSingle_title.addEventListener('click', () => {
  if ('active' === selectSingle.getAttribute('data-state')) {
    selectSingle.setAttribute('data-state', '');
  } else {
    selectSingle.setAttribute('data-state', 'active');
  }
});

for (let i = 0; i < selectSingle_labels.length; i++) {
  selectSingle_labels[i].addEventListener('click', (evt) => {
    selectSingle_title.textContent = evt.target.textContent;
    selectSingle.setAttribute('data-state', '');
  });
}

// Настройка мобильного фильтра //

let open_filter = document.querySelector('.filter-mobile');
let mobile_filter = document.querySelector('.mobile-aside-filter-wrapper'); 
let close_filter = document.querySelector('.filter-close__btn'); 

open_filter.onclick = () => { 
  mobile_filter.classList.toggle('mobile-aside-filter-wrapper--active'); 
}

close_filter.onclick = () => { 
  mobile_filter.classList.remove('mobile-aside-filter-wrapper--active'); 
}

// Настройка ползунка  ///

const rangeInput = document.querySelectorAll(".range-input input"),
priceInput = document.querySelectorAll(".price-input input"),
range = document.querySelector(".slider .progress");
let priceGap = 1000;

priceInput.forEach(input =>{
    input.addEventListener("input", e =>{
        let minPrice = parseInt(priceInput[0].value),
        maxPrice = parseInt(priceInput[1].value);
        
        if((maxPrice - minPrice >= priceGap) && maxPrice <= rangeInput[1].max){
            if(e.target.className === "input-min"){
                rangeInput[0].value = minPrice;
                range.style.left = ((minPrice / rangeInput[0].max) * 100) + "%";
            }else{
                rangeInput[1].value = maxPrice;
                range.style.right = 100 - (maxPrice / rangeInput[1].max) * 100 + "%";
            }
        }
    });
});

rangeInput.forEach(input =>{
    input.addEventListener("input", e =>{
        let minVal = parseInt(rangeInput[0].value),
        maxVal = parseInt(rangeInput[1].value);

        if((maxVal - minVal) < priceGap){
            if(e.target.className === "range-min"){
                rangeInput[0].value = maxVal - priceGap
            }else{
                rangeInput[1].value = minVal + priceGap;
            }
        }else{
            priceInput[0].value = minVal;
            priceInput[1].value = maxVal;
            range.style.left = ((minVal / rangeInput[0].max) * 100) + "%";
            range.style.right = 100 - (maxVal / rangeInput[1].max) * 100 + "%";
        }
    });
});


// Настройки модального окна // 


function bindModal(trigger, modal, close) {
  trigger = document.querySelectorAll(trigger),
  modal = document.querySelector(modal),
  close = document.querySelector(close)

  btns = trigger;
  btns.forEach(function (el) {
    el.addEventListener('click', () => {
      modal.style.display = 'flex';
    });
  })

  close.addEventListener('click', () => {
    modal.style.display = 'none';
    modal.querySelectorAll('.input').forEach(function(el) {
      el.value = 0;
    });
  });

  modal.addEventListener('click', (e) => {
    if (e.target == modal) {
      modal.style.display = 'none';
    }
  })
}

bindModal('.modal_btn', '.modal_wrapper', '.modal_close')

const counterWrappers = document.querySelectorAll('.add-btn__wrapper');

counterWrappers.forEach(function (el) {
  const btnIncrease = el.querySelector('.increase');
  const btnDecrease = el.querySelector('.decrease');
  const counterInput = el.querySelector('.input');

  btnIncrease.addEventListener('click', function () {
    counterInput.value = increase(counterInput.value);
  });
  btnDecrease.addEventListener('click', function () {
    counterInput.value = counterInput.value <= 0 ? counterInput.value : decrease(counterInput.value);
  });
});

function increase(value) {
  return +value + 1;
}

function decrease(value) {
  return +value - 1;
}

// Настройка бургера //

document.querySelector('.mobile-catalog__btn').addEventListener('click', () => {
  document.querySelector('.header__burger-catalog').classList.toggle('burger--active');
  document.querySelector('.burger-btn').classList.toggle('burger-btn--active');
});

document.querySelector('.catalog__btn').addEventListener('click', () => {
  document.querySelector('.header__burger-catalog').classList.toggle('burger--active');
  document.querySelector('.burger-desktop').classList.toggle('burger-btn--active');
});

// Удаление всех :checked //

document.getElementById("reset-button").addEventListener("click", function(e) {
  var formBlock = document.getElementById("aside-filer__list");
  var inputArr = formBlock.querySelectorAll("input[type=checkbox]");
  inputArr.forEach(function(el) {
    el.checked = false;
  });
});