.header {
    margin-bottom: 40px;

    .header__mobile {
        display: none;
    }

    @media (max-width: 996px) {
        .header__mobile {
            display: block;

            .mobile__menu {
                display: flex;
                justify-content: space-between;
                align-items: center;
                box-shadow: 0px 2px 6px rgba(37, 40, 43, 0.15);

                .mobile__logo {
                    flex: 0 1 80%;
                }

                .menu__user {
                    display: flex;
                    align-items: center;
                }
            }
        }
    }

    @media (max-width: 450px) {

        .header__mobile {
            .mobile__logo {
                flex: 0 1 45% !important;
            }
        }
    }

    .header__top {
        .header__wrapper {
            display: flex;
            justify-content: space-between;
            align-items: center;

            .header__geo {
                display: flex;
                flex-direction: column;
            }

            .header__search {
                max-width: 372px;
                width: 100%;
                position: relative;
                &:after {
                    position: absolute;
                    content: url('../img/search.svg');
                    right: 12px;
                    bottom: 6px;
                }      
            }

            @media (max-width: 1280px) {
                .header__search {
                    max-width: 200px;
                }
            }

            .header__social-network {
                .social-network:not(:last-child) {
                    padding-right: 10px;
                }
            }
        }
    }

    @media (max-width: 996px) {
        .header__top {
            display: none;
        }
    }

    .header__bottom {
        background: #F2F2F2;

        .header__menu-list {
            display: flex;
            justify-content: space-between;
            align-items: center;

            .menu__items {
                margin-left: -60px;
            }

            @media (max-width: 1280px) {
                .menu__items {
                    margin-left: 0;
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: center;
                    align-items: flex-end;
                    
                    a {
                        line-height: 2;
                    }
                }
            }
              

            .menu__user {
                display: flex;
                align-items: center;

                .btn-user {
                    padding-right: 24px;
                    cursor: pointer;
                }
            }
        }
    }

    @media (max-width: 996px) {
        .header__bottom {
            display: none;
        }
    }

    .header__burger-catalog {
        margin-top: 10px;
        display: none;
       
        .burger-catalog__list {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            box-shadow: 0 10px 20px rgb(0 0 0 / 25%);
            padding: 15px;
            margin: 0 -5px;

            .burger-catalog__item {
                // margin-bottom: 20px;
                flex: 1 1 auto;
                margin: 10px 10px;
            }
        }
    }

    .burger--active {
        display: flex;
    }
}

.main {
    margin-bottom: 200px;

    .breadcrumbs {
        margin-bottom: 50px;
    }

    .section__winter-overalls {
        .section__tags {
            display: flex;
            flex-wrap: wrap;
            margin-bottom: 10px;
        }

        .catalog-sorting {
            display: flex;
            justify-content: flex-end;
            margin-bottom: 20px;
        }

        .mobile-catalog-filter {
            display: none;
        }

        .catalog-content {
            .catalog-wrapper {
                display: flex;

                

                .catalog-aside {
                    // height: 100%;
                    min-width: 256px;
                    padding: 33px 20px;
                    background: #F9F9FA;
                    border: 1px solid #E8E8E8; 

                
                    .aside-filter__item {
                        margin-bottom: 40px;
                    }

                    .aside-buttons {
                        display: flex;
                        flex-direction: column;
                    }
                }

                .mobile-aside-filter-wrapper {
                    height: 100%;
                    background: #f9f9fa;

                    .mobile-filter-close {
                        display: none;
                    }
                }

                @media (max-width: 807px) {

                    .mobile-aside-filter-wrapper {
                        height: auto;
                        position: absolute;
                        display: none;
                        width: 100%;
    
                        .mobile-filter-close {
                            display: block;
                        }

                        .catalog-aside { 
                            display: block;
                            background: #f9f9fa !important;
                        } 
                    }

                    .mobile-aside-filter-wrapper--active {
                        right: 0;
                        display: block;
                        transition-property: all;
                    }
                }


                .catalog-card__list {
                    display: flex;
                    flex-wrap: wrap;
                    

                    .catalog-card__item {
                        min-width: 256px;
                        max-height: 703px;
                        width: 100%;
                        flex: 1 0 25%;
                        background: #FFFFFF;
                        border: 1px solid #E8E8E8;

                            &:hover {
                                box-shadow: 0px 0px 20px rgba(37, 40, 43, 0.2);
                            }
                        
                        .card-item-content {
                            padding: 24px 20px;

                            .card-item-buttons {
                                display: flex;
                                justify-content: space-between;
                            }
                        }
                    }
                }

                .catalog-card__pagination {
                    margin-top: 30px;
                    .pagination__list {
                        display: flex;
                        align-items: center;
                        justify-content: center;
                    }

                }
            }

            @media (max-width: 807px) {
                position: relative;
            }
        }
    }

    @media (max-width: 807px) {
        .mobile-block__wrapper {

            display: flex;
            justify-content: space-between;

            
            .mobile-catalog-filter {
                display: block;
                padding-right: 4px;
            }
        }
    }
}

.footer {
    background: #F9F9FA;

    .footer__top {
        padding-top: 52px;
        padding-bottom: 50px;

        .footer__list {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;

            .footer__item {
                flex: 0 1 20%;
                min-width: 298px;
                width: 100%;
            }

            @media (max-width: 1211px) {
                .footer__item {
                    flex: 0 1 35%;
                    
                }
                
                #first {
                    order: 3; 
                }

                #second {
                    order: 1;
                    margin-bottom: 50px;
                }

                #third {
                    order: 2;   
                }

                #fourth {
                    order: 4;
                    
                }
            }

            @media (max-width: 615px) {
                .footer__item {
                    flex: 0 1 100%;
                }

                #second {
                    order: 1;
                    margin-bottom: 20px;
                }

                #third {
                    order: 2;
                    margin-bottom: 20px;   
                }
            }
        }

        @media (max-width: 1211px) {
            .footer__list {
                justify-content: space-evenly;
            }
        }
    }

    .footer__bottom {
        border-top: 1px solid #E8E8E8;
    }
}
